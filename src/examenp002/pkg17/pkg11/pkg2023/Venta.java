/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenp002.pkg17.pkg11.pkg2023;

/**
 *
 * @author Iker Martinez
 */
public class Venta {
    private int id;
    private int tipo;
    private float cantidad; // premiu 24.50; regular 20.50

    public Venta() {
        id = 0;
        tipo =0;
        cantidad = 0.0f;
    }
    
    public Venta(int id, int tipo, float cantidad) {
        this.id = id;
        this.tipo = tipo;
        this.cantidad = cantidad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }
    
    
    
    public float precioLitro(){
    float pl =0.0f;
    if (tipo == 2){pl = 24.50f;}
    if (tipo == 1){pl = 20.50f;}
        return pl;
    }
    
    public float totalP(){
    float total;
    float pl = this.precioLitro();
    total =  pl * cantidad;
    return total;
    }

    void setCantidad() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
